import abc

import model

class AbstractRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, batch: model.Batch):
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, reference):
        raise NotImplementedError 

class SqlAlchemyRepository(AbstractRepository):
    def __init__(self, session) -> None:
        super().__init__()
        self.session = session
    
    def add(self, batch):
        self.session.add(batch)
    
    def get(self, reference):
        pass

class SqlRepository(AbstractRepository):
    def __init__(self, session) -> None:
        super().__init__()
        self.session = session
    
    def add(self, batch) -> None:
        self.session.execute(
            'INSERT INTO "batches"'
            '(reference, sku, _purchased_quantity, eta) '
            'VALUES (:reference, :sku, :_purchased_quantity, :eta);',
            dict(reference = 
                batch.reference, sku = batch.sku, 
                _purchased_quantity = batch._purchased_quantity, 
                eta = batch.eta
                )
        )
        
    
    def get(self, reference) -> model.Batch:
        pass