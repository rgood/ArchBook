from datetime import date
from model import OrderLine, Batch

def make_batch_and_order(sku: str, avail_qty: int, order_qty: int):
    return(
        Batch("batch1", sku, avail_qty, eta = date.today()),
        OrderLine("order-ref", sku, order_qty)
    )


def test_allocate_order_reduces_avail_qty():
    aBatch, anOrder = make_batch_and_order("RED-CHAIR", 20, 2)
    aBatch.allocate(anOrder)
    assert aBatch.available_quantity == 18

def test_can_allocate_if_available_quantity_greater_than_order_qty():
    largeBatch, smallOrder = make_batch_and_order("RED-CHAIR", 20, 2)
    assert largeBatch.can_allocate(smallOrder)

def test_cannot_allocate_if_available_quantity_less_than_order_qty():
    smallBatch, largeOrder = make_batch_and_order("BLUE-CUSHION", 1, 2)
    assert smallBatch.can_allocate(largeOrder) is False

def test_can_allocate_if_available_quantity_equal_to_order_qty():
    largeBatch, smallOrder = make_batch_and_order("RED-CHAIR", 20, 2)
    assert largeBatch.can_allocate(smallOrder)

def test_cannot_allocate_different_sku():
    lampBatch = Batch("lamp-ref", "RED-LAMP", 20, eta = None)
    chairOrder = OrderLine("order-1", "BLUE-VASE", 2)
    assert lampBatch.can_allocate(chairOrder) is False

def test_can_only_deallocate_existing_order():
    aBatch, unallocated_order = make_batch_and_order("CHAIR", 20, 10)
    aBatch.deallocate(unallocated_order)
    assert aBatch.available_quantity == 20

def test_only_allocate_order_once():
    aBatch, anOrder = make_batch_and_order("BLUE-VASE", 10, 2)
    aBatch.allocate(anOrder)
    aBatch.allocate(anOrder)
    assert aBatch.available_quantity == 8
