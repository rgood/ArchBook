import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from db_tables import metadata

#ORM imports
from sqlalchemy.orm import clear_mappers, sessionmaker
from sqlalchemy.sql.schema import MetaData
from orm import metadata, start_mappers


@pytest.fixture #define a function to use in testing
def in_mem_db():
    engine = create_engine('sqlite:///:memory:')
    metadata.create_all(engine)
    return engine

@pytest.fixture
def session(in_mem_db):
    yield sessionmaker(bind=in_mem_db)()


# fixture used for the ORM implementation
@pytest.fixture
def orm_session(in_mem_db):
    start_mappers()
    yield sessionmaker(bind=in_mem_db)()
    clear_mappers()
