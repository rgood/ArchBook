import sqlalchemy
import model
import repository

def test_repository_can_save_record(session):
    aBatch = model.Batch("testBatch", "GREY-TABLE", 112, eta = None)

    repo = repository.SqlRepository(session)
    repo.add(aBatch)
    session.commit()

    rows = list(session.execute('SELECT reference, sku, _purchased_quantity, eta FROM "batches"'))
    assert rows == [("testBatch", "GREY-TABLE", 112, None)]

    