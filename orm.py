from sqlalchemy.orm import mapper, relationship
from sqlalchemy.sql.expression import null
from sqlalchemy.sql.schema import Column, ForeignKey, MetaData, Table
from sqlalchemy.sql.sqltypes import Date, Integer, String

import model 
# this is the dependency inversion, the ORM depends on the model, not other way around

metadata = MetaData()

order_lines = Table(
    'order_lines', metadata,
    Column('id', Integer, primary_key=True),
    Column('sku', String(255)),
    Column('qty', Integer, nullable=False),
    Column('orderid', String(255))
)
#was not able to pass test originally because orderid above was spelled order_id
#I guess the mapper has to have attributes with name matches.  
#changeing to the above, which matches the class OrderLine, fixed the tests
batches = Table(
    'batches', metadata,
    Column('id', Integer, primary_key=True),
    Column('reference', String(255)),
    Column('sku', String(255)),
    Column('_purchased_quantity', Integer),
    Column('eta', Date, nullable=True)
)

allocations = Table(
    '_allocations', metadata,
    Column('id', Integer, primary_key=True),
    Column('orderline_id', ForeignKey('order_lines.id')),
    Column('batch_id', ForeignKey('batches.id'))
)

def start_mappers():
    lines_mapper = mapper(model.OrderLine, order_lines)
    mapper(model.Batch, batches, properties={
        '_allocations': relationship(
            lines_mapper,
            secondary=allocations,
            collection_class=set,
        )
    })
    #sqlalchemy is binding the model classes to the table schema

'''
SQLalchemy 'declarative style', copied straiht from the book
and example of severe dependency coupling of the model to the DB layer

from sqlalchemy import Column, ForeignKey, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()

class Order(Base):
    id = Column(Integer, primary_key=True)

class OrderLine(Base):
    id = Column(Integer, primary_key=True)
    sku = Column(String(250))
    qty = Integer(String(250))
    order_id = Column(Integer, ForeignKey = 'order.id')
    order = relationship(Order)

class Allocation(Base):
    pass
'''

