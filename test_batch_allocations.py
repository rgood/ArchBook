from datetime import date, timedelta
import pytest
from model import OrderLine, Batch, OutOfStock, allocate

today = date.today()
tomorrow = today + timedelta(days=1)
nextweek = today + timedelta(days=10)

def test_allocation_uses_current_stock_over_shipment():
    current_stock = Batch("current-stock", "GAUDY-TABLE", 100, eta = None)
    shipment_stock = Batch("shippment-stock", "GAUDY-TABLE", 100, eta = today)
    line = OrderLine("anOrder", "GAUDY-TABLE", 10)
    allocate(line, [current_stock, shipment_stock])

    assert current_stock.available_quantity == 90
    assert shipment_stock.available_quantity == 100

def test_use_earliest_shipment():
    earliest_stock = Batch('early', 'GAUDY-TABLE', 100, eta = today)
    middle_stock = Batch('mid', 'GAUDY-TABLE', 100, eta = tomorrow)
    late_stock = Batch('late', 'GAUDY-TABLE', 100, eta = nextweek)
    line = OrderLine("anOrder", "GAUDY-TABLE", 10)

    allocate(line,
    [late_stock, earliest_stock, middle_stock]
    )
    assert earliest_stock.available_quantity == 90
    assert middle_stock.available_quantity == 100
    assert late_stock.available_quantity == 100

def test_allocation_returns_correct_reference():
    current_stock = Batch("current-stock", "GAUDY-TABLE", 100, eta = None)
    shipment_stock = Batch("shipment-stock", "GAUDY-TABLE", 100, eta = today)
    line = OrderLine("anOrder", "GAUDY-TABLE", 10)

    ref = allocate(line, [current_stock, shipment_stock])
    assert ref == current_stock.reference 

def test_raises_out_of_stock_exception():
    aBatch = Batch("shipment-stock", "GAUDY-TABLE", 100, eta = today)
    line = OrderLine("Order-1", "GAUDY-TABLE", 100)
    allocate(line, [aBatch])

    with pytest.raises(OutOfStock, match="GAUDY-TABLE"):
        allocate(OrderLine('Order-2', "GAUDY-TABLE", 10), [aBatch])   