from __future__ import annotations
from dataclasses import dataclass
from datetime import date
from typing import Optional, List, Set
    #typing is for data types? not keystrokes?

class OutOfStock(Exception):
    pass
    #make new child of exception, does nothing?

@dataclass(unsafe_hash=True) #make value object and immutable
class OrderLine:
    orderid: str
    sku: str
    qty: str

def allocate(line: OrderLine, batches: List[Batch]) -> str: 
    #the '->' defines the return type
    try:
        batch = next(
            b for b in sorted(batches) if b.can_allocate(line)
        )
        #next returns the first item from the sorted list. 
        #  
        # How does sorted know to sort on ETA and not qty?
        # --> because we implemented the dunder method (magic methods) __ge__
        # Which is what defines the sorting

        batch.allocate(line)
        return batch.reference
    except StopIteration:
        raise OutOfStock(f'Out of stock for sku {line.sku}') 
        #f at the beginning is a string formatter, new style over %s and arguments outside of string


class Batch:
    # init method defines the 'constructor'
    def __init__(
        self, ref: str, sku: str, qty: int, eta: Optional[date]
    ):

    #set object attributes in the constructor.  no defaults here
        self.reference = ref
        self.sku = sku
        self.eta = eta
        self._purchased_quantity = qty
        self._allocations = set() # type: Set[OrderLine]

    #### Method overrides ####

    def __repr__(self): #object representation string, for debugging purposes
        return f'<Batch {self.reference}>' 
        #f is fprintf with less formatting, can inline the injected value
    
    def __eq__(self, other):
        if not isinstance(other, Batch):
            return False
        return other.reference == self.reference
    
    def __hash__(self):
        return hash(self.reference)
    
    def __gt__(self, other):
        if self.eta is None:
            return False
        if other.eta is None:
            return True
        return self.eta > other.eta

    ####  Accessors  ####
    # These are property decorators, define aliases for get methods

    @property
    def allocated_quantity(self) -> int:
        return sum(line.qty for line in self._allocations)
        # add up all allocated qty in the set

    @property
    def available_quantity(self) -> int:
        return self._purchased_quantity - self.allocated_quantity
        # call the previously defined property

    #### Methods ####

    def can_allocate(self, line: OrderLine) -> bool:
        return self.sku == line.sku and self.available_quantity >= line.qty

    def allocate(self, line: OrderLine):
        if self.can_allocate(line):
            self._allocations.add(line) # allocations is type Set
    
    def deallocate(self, line: OrderLine):
        if line in self._allocations:
            self._allocations.remove(line)
        

