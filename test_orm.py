from datetime import date

import model

# some throw away tests to get going
def test_orderline_mapper_can_load_lines(orm_session):
    
    orm_session.execute(
        'INSERT INTO order_lines (orderid, sku, qty) VALUES '
        '("ORDER-1", "RED-CHAIR", 12),'
        '("ORDER-1", "RED-TABLE", 13),'
        '("ORDER-2", "BLUE-LIPSTICK", 14);' 
    )
    
    expected = [
        model.OrderLine("ORDER-1", "RED-CHAIR", 12),
        model.OrderLine("ORDER-1", "RED-TABLE", 13),
        model.OrderLine("ORDER-2", "BLUE-LIPSTICK", 14),
    ]
    assert orm_session.query(model.OrderLine).all() == expected
    
def test_can_save_order_to_new_record(orm_session):
    new_record = model.OrderLine("test-order", "RED-DESK", 10)
    orm_session.add(new_record)
    orm_session.commit()

    rows = list(orm_session.execute('SELECT orderid, sku, qty FROM "order_lines"'))
        #had to add the exact order to the query, was coming out backwards
    assert rows == [("test-order", "RED-DESK", 10)]
